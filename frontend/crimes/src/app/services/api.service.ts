import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrimeSearchResponse } from '../models/CrimeSearchResponse';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class APIService {
  constructor(private http: HttpClient) {}

  search(address: string, date: string = ''): Observable<CrimeSearchResponse> {
    const searchURL = '/api/search'; // FIXME: enumerate
    return this.http
      .get(searchURL, { params: { address, date } })
      .pipe(map((raw: any) => new CrimeSearchResponse(raw)));
  }
}
