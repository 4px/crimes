import { Component, Input } from '@angular/core';
import { Crime } from '../models/Crime';

@Component({
  selector: 'app-crimes-list',
  templateUrl: './crimes-list.component.html',
  styleUrls: ['./crimes-list.component.scss']
})
export class CrimesListComponent {

  @Input() crimes: Crime[];

  columns: string[] = ['id', 'category', 'lat', 'lng', 'street', 'outcome'];
}
