// globals
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';

import * as moment from 'moment';
import { Moment} from 'moment';

// services
import { APIService } from './services/api.service';

// components & models
import { CrimesListComponent } from './crimes-list/crimes-list.component';
import { CrimeSearchResponse } from './models/CrimeSearchResponse';

// animations
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations'; // this is just insane...


export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

  animations: [
    // the fade-in/fade-out animation.
    trigger('simpleFadeAnimation', [

      // the "in" style determines the "resting" state of the element when it is visible.
      state('in', style({ opacity: 1 })),

      // fade in when created. this could also be written as transition('void => *')
      transition(':enter', [
        style({ opacity: 0 }),
        animate(300)
      ])
    ])
  ],

  providers: [
    // I've copy-pasted this entire thing, and I can't believe how hard it is to change a simple date format :)
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class AppComponent {
  constructor(private api: APIService) {}

  samples = [
    { address: '37 Carr Lane, Leeds (Rawdon), LS19 6PD Rawdon Leeds England United Kingdom' },
    { address: '37 Carr Lane, Leeds (Rawdon), LS19 6PD Rawdon Leeds, United Kingdom', date: moment().year(2018).month(5) },
    { address: '61 King Street, Leicester, LE1 6RJ Leicester, United Kingdom' },
    { address: '61 King Street, Leicester, LE1 6RJ Leicester, United Kingdom', date: moment().year(2017).month(0) },
    { address: 'unknown address missing from google maps' },
    { address: 'Sofia, Bulgaria' }
  ];

  searchForm = new FormGroup({
    address: new FormControl('', [Validators.required]),
    date: new FormControl()
  });

  loading: boolean;
  result: CrimeSearchResponse = new CrimeSearchResponse();

  chosenYearHandler(selectedDate: Moment) {
    const localDate = this.searchForm.get('date').value || moment();
    localDate.year(selectedDate.year());
    this.searchForm.get('date').setValue(localDate);
  }

  chosenMonthHandler(selectedDate: Moment, datepicker: MatDatepicker<Moment>) {
    const localDate = this.searchForm.get('date').value;
    localDate.month(selectedDate.month());
    this.searchForm.get('date').setValue(localDate);
    datepicker.close();
  }

  performSearch() {
    // for the sake of simplicity we don't do any checks here, as the seach button should be automatically disabled if form is invalid
    const searchDate = this.searchForm.get('date').value ? this.searchForm.get('date').value.format('YYYY-MM') : '';

    this.loading = true;
    this.searchForm.disable();

    const doneLoading = () => {
      this.loading = false;
      this.searchForm.enable();
    };

    this.api.search(this.searchForm.get('address').value, searchDate).subscribe((response: CrimeSearchResponse) => {
      this.result = response;
      doneLoading();
    }, (err) => {
      console.warn('App :: performSearch ERROR -> ', err);
      this.result.fail('Sorry, something went wrong, please try again!');
      doneLoading();
    });
  }

  populateFormSample(index) {
    this.searchForm.patchValue(this.samples[index]);
  }
}
