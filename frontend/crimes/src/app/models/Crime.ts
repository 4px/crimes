import { Injectable } from '@angular/core';

export class Crime {
  id: number;
  category: string;

  // location
  location: {
    latitude: string;
    longitude: string;
    street: string;
  };

  outcome: string;

  constructor(raw?: any) {
    Object.assign(this, {
      id: raw.id,
      category: raw.category,
      location: {
        latitude: raw.location.latitude,
        longitude: raw.location.longitude,
        street: raw.location.street.name
      },
      outcome: raw.outcome_status ? raw.outcome_status.category : ''
    });
  }
}
