import { Crime } from './Crime';

export class CrimeSearchResponse {
  success: boolean;
  latlng: {
    lat: string,
    lng: string
  };
  message: string;
  debug: string;
  crimes: Crime[];

  constructor(raw?: any) {
    if (!raw) {
      return;
    }
    this.success = raw.success;
    this.latlng = raw.latlng;
    this.debug = raw.debug;
    this.message = raw.message;
    this.crimes = raw.crimes ? raw.crimes.map((crime: any) => new Crime(crime)) : [];
  }

  reset() {
    Object.assign(this, {
      success: false,
      latlng: {
        lat: '',
        lng: ''
      },
      message: '',
      debug: '',
      crimes: []
    });
  }

  fail(message: string) {
    this.reset(); // just in case
    this.message = message;
  }
}
