# Crime Search Project

Thank you for the task! I've spent a very interesting few days, stumbled upon things I had to learn and mostly had fun!

# Understanding & Planning

## Getting the task

There were a few unknowns, but guessing that was part of the task, I decided I could figure it out by myself and went forward with my interpretation. Hope I got it right :) I ended up with a few ideas:

1. The police API requires lat/lng for querying their database. Since the user only has an address input field on the frontend side, I decided to use Google's Geocoding servies to convert address to location. That comes with a bit of a risk, that the results are not precise or the convertion could not happen.

2. All of the logic could be done on the front end only, as there are SDKs for the services in need. Since the requirements are to use EC2 and Node, I decided to implement a backend service for the search. It will combine the two queries behind the scene - geocoding the address and using the police API.

3. Installing MongoDB was optional, but I didn't see it fit for the job, so the backend I am making would be purely a wrapper over geocoding and police API services.

4. Since EC2 was a requirement, I decided to put both FE and BE code on the same instance, meaning they would reside in the same repository. That's something I'm not used to, but seems a reasonable and fairly simple solution. I went with simple packages structure, meaning single package.json at root level and both ends in separate folders (but not packages). The backend should serve the frontend build as a static website.

5. Installing git was optional, and it was one of the easy ways to get some kind of deployment procedure. I've used it in the past for either manual pull/restart or with git hooks. Since I haven't done exactly Bitbucket to EC2 CD I decided to go with that - I knew the steps but I can definitely learn a lot. Ideally the pipeline would run all lints and tests (on both ends), zip and send the project to S3, then ping the EC2 instance (through CodeDeploy) so it can get the image and deploy it. The instance itself would build the frontend code and run the backend server, providing API endpoints and serving the built FE.

## Planning

The goal was fairly straightforward, and I had a simple plan:

1. Create project skeleton using Angular's CLI and Express generator. Use both of them as standalone apps, but pave the way for backend to serve a prebuilt frontend app as a static website
2. Make the backend serve the frontend as static website
3. Create a simple markup
4. Make the simple markup call a backend code using the input field
5. Implement backend logic using both 3rd party services
6. Implement logic in frontend, pinging the actual backend service with real results
7. Prettify the markup on the front side
8. Setup CI/CD

## Concerns & Estimation

1. My main concern was CI/CD, because I planned to use both ends in the same repo. It seems I've spent quite a lot of time in the FE world, and making CD to EC2 is something I haven't done before. Putting a prebuilt code to S3 to be served is fairly simple :)
2. Angular is something I've just played with before. While the task is simple enough, getting everything to work in an eye-pleasing way might cause troubles.

My initial estimation was **about two days** but I would put three just to be sure.


# Improvements & things to keep in mind

While things work, there are plenty of pieces I would definitely like to change. Some of them are results of decisions I've taken, some of because of simplicity reasons, and some are just failures.

Overall there are many areas of possible improvements.

1. FE tests are not working and are not part of the pipeline. I've already described that in the timeline section. I feel that's something important and would definitely not skip it in real life.

2. The backend services is pretty basic and responses are basic as well. They are always valid JSON (status 200 with `success: false` property inside). It does not follow RESTful principles, but does the work in a simplest way. Query parameters are passed to a GET handler, which enabled easy testing :P Errors are handled by directly providing error messages that are about to be displayed on the frontend. I would typically let the frontend provide texts based on error codes, which would help possible future translations.

3. Backend test are basic and do not cover everything. As people say - there are never too many tests :) For the sake of simplicity I decided to write just a few basic ones without spending too much time on that.

4. There is no linter configured. I am not using TS regularly so tslint was something I did at the end of my task, and I have eslint configured on IDE level that I turn on/off per project. FE linting is part of the pipeline, but BE has none currently. I definitely dislike that.

5. Shell scripts look like copy-pasted spaghetti code, because some parts of them indeed are :( While I know my goal, I am not familiar with all of the commands and for some of them I just try if it works. Not entirely sure about the side effects that might be produced, and would definitely like to spend some time learning more.

6. I find FE code messy. I've described my Angular pains below. I would usually split code into multiple components even for representational reasons. In my code I feel like I have a single "mother of it all" component with everything inside. It looks a bit ugly to me, and it's something I would surely improve.

7. I haven't followed the Git Flow workflow. Since I left CI/CD for last, and I'm the only person working on the project, I decided there would be no real benefit of making branches and accepting my own PRs.

8. I haven't setup different branches with different pipelines and deployments for that. Typically I'd go with running lints/test for branches and PRs, requiring at a status check for merge. Having a test/stagin env is also something I'm used to.

9. Pipeline does not wait for server deployment. It pings CodeDeploy and waits for a deployment to be created, but does not wait for its result. The reason is that I had a few failures with EC2 (described below) and waiting for the in the pipeline was wasting my minutes :) That's something I would **definitely** have in real environment.

10. I couldn't find a simple way to set up env variables on EC2. I am used to having `.env` files, but since the entire app folder is getting rewritten, I didn't know where to put those. I've created a file for that and have a shell script to export its content, but that's definitely ugly and something I'd do in a proper way.

11. Make the code simpler and cleaner, especially the frontend one. Things like the router are left there for no reason. I could surely spend a few minutes doing that, but at the same time I could spend hours polishing it. I decided there are too many things to polish and I didn't want to go to that path. As a fast test assignment I do believe making things work in a reasonable manner is good enough. Or at least I hope so :P

12. ~~There is currently no frontend responsiveness by any means :(~~ I've made a few very minor responsive adjustments as I couldn't left it like that. But as a frontend person that belives UX is one of the most imporant parts of a product, I fell in the coding trap and completely forgot about that.


# Timeline of events + thoughts

## Monday, 13th

1. I knew the file structure I needed, but I had to play with the cli a few times to get the right params in order to make that work.
2. I needed env variables, which are typically supported by webpack. Unfortunately Angular does not have that up and running, and uses simple `environments/environment.prod.ts` file. Basically all your variables are inside the code :( Will have to go with that for now.
3. Working with dates and Material is just a pure pain.
4. Calling HttpClient's get method does nothing. It needs listener attached in order to actually make the call :|
5. NgModule seems to be the mother of it all. I surely need to read a bit more about project structure and proper code splitting.

I'd say it took me quite some time reading documentation and getting up to speed with things. I felt slow. At the end of the day I was able to achieve:
- Setup the base project structure
- Get the backend to serve the frontend when needed
- Create basic markup
- Create basic backend route that returns data
- Connecting the two, so that frontend pings the backend and gets some sample data in return

## Tuesday, 14th

1. The only place the backend was making http calls was pingin the police API. So I was using simple node https, but I decided to ditch it. Mostly because the police API is throwing errors if the month is not available, but proper error handling on json expected response stars to be pain. Axios has it all working for me.

2. The police API is awkward to use - it throws 404 if you provide a month that (probably) they don't have. Not only in the future (June 2020) but in the past as well (March 2016). Combining this with the Geocoding service and error handle it all was strange.

3. I was happy to spend an hour doing some very basic backend unit tests. Surely they don't cover anything major, but for the sake of simplicity I didn't dive too deep in that area. Angular and CI/CD is waiting, so I decided to come back later to this.

4. I spent some time with Angular today. It's super flexible, but split into millions of pieces. It needs a lot of reading to get it stared. Documentation is the worst compared to React and Vue. Examples are full blown projects, full of unrelevant and misleading code. There's a lot of magic going on in different places that is hard to grasp. I like the concepts, but am pretty new to like the implementation. It's always way deeper than what I've seen. I've read a bit more about folder structure (modules); components interaction; data flow; reactivity; custom services, pipes and directives and more. I realized I won't be able to use all of that, and have to make some very very basic solution for now. What I liked is the compile time in dev mode :D

5. Another point on Angular. This was the biggest time consumer, and I would say I am **shocked** by how complex some things are. A few examples would be using dates in Angular Material (okay, not engirely Angular's fault, but it's an official library). Working with HTTP service is also a pain - I don't know why they even bothered reinventing the wheel with that. Returning observables is strange, but converting your response to a specific model strucute was even stranger. My guess is that the samples I've found are not the proper way to use Angular.

6. I made things work, even though ugly. I decided to go to the last hard part before moving on to making things pretty. I knew all the bits and pieces of the things I needed with AWS, but I haven't made this exact pipeline to work. A lot of reading and playing around. All felt quite okay for the very few hours I've spent on.

Today I realized I felt too optimistic about my concerns. Angular, TypeScript and AWS are things I have knowledge with, but I have a lot to learn! Problems are easy, but solutions are not well documented (too broad, too scarce or too outdated).

Biggest time wasters for today were Angular and Linux :) While setting up AWS parts was easy, getting things to run on Linux was awkward (CodeDeploy agent said it started successfully, but exited).

I was able to:
- Implement backend search logic (wrapping both serviced)
- Make very basic backend unit tests
- Implement frontend search logic
- Test very basic Bitbucket pipeline CI+CD using CodeDeploy

## Wednesday, 15th

1. "Since the table optimizes for performance, it will not automatically check for changes to the data array." Angular Material.
2. I scratched the surface with Angular's forms. Unfortunately, they are again split into so many pieces it's hard to grasp in a an hour. I've implemented a very basic validations and form/data flow. Not very clever solutions on error handling.
3. The **worst** thing of the day is that I couldn't get Angular tests to work. Even using `ng generate` with very basic markup changes - tests wouldn't work. I decided to skip that for later, but it turned out I still can't get to know what's the real reason. Build passes, everything works correctly. The code of the sample tests is a nighmare to read...
4. I made things a bit prettier on the FE side, and made a few touches here and there to improve the overall code quality.

Here I can say I made a pretty awful rookie mistake - I thought since I had CI/CD working the previous day with a basic project, I would just update my configs and make it work in the real repo. And because I got tired of fighting with Angular, I spent half of the day off, leaving the pipelines for the evening as a half an hour task.

What I got was a set of nightmares. Pipeline build was failing - I included `ng lint` as a step, but Angular CLI requires higher node version. Switched that easily, but got to know that the node image does not have `zip`, so I couldn't create the artifact to be uploaded to S3. Got a nasty fix to always `apt-get install` it :( Then I couldn't build the frontend code in production mode on the EC2 instance, as t2.micro wasn't powerful enough. I've had this issue before with Vue, but didn't remember the fix. Found another nasty solution to use a swap for that. Then I had problems with permissions and executing scripts. Environmental variables are just a dream. So my easy "half an hour finalization" turned out to working till 8:30 - way past my normal working hours (I'm an old man).

## Summary

Because of the last day, I would say I am not satisfied with my result. While I tried to follow some good practices, I felt short on keeping all of them in mind and I consider the end result a mess. I couldn't get FE tests to work, my shell scripts are full of crap, and while everything works, I find it ugly. Please read the next section.