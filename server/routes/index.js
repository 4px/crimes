const express = require('express');
const debug = require('debug')('crimes:routes:index');

const CrimesService = require('../services/CrimesService');
const GeocodingService = require('../services/GeocodingService');

const router = express.Router();

/**
 * Simple method for easy responding. Passes proper data structure to `res.json`.
 * 
 * The method returns back the query params as a response, for validation purposes.
 * 
 * @param {*} res         - the received request's response (express middleware)
 * @param {*} address     - the address the user is trying to query
 * @param {*} date        - the date query param
 * @param {*} latlng      - the geocoding response from Google services, based on address
 */
const prepareResponse = (res, address, date) => ({
  success(crimes, latlng) {
    res.json({ success: true, crimes, address, date, latlng });
  },
  /**
   * Response fails for some reason - we still return 200, but with `success: false`.
   * @param {*} message   - human readable message explaining the error
   * @param {*} debug     - debug message, most often error stack one
   */
  failure(message = '', debug = '') {
    res.json({ success: false, address, date, message, debug });
  }
});


router.get('/search', async (req, res, next) => {
  const { address, date = '' } = req.query; // unwrap query params
  const respond = prepareResponse(res, address, date); // prepare json wrapper

  if (!address) {
    debug(':: /search ERROR -> address query missing');
    return respond.failure('Address is missing');
  }


  // step 1: geocode
  let latlng;
  try {
    latlng = await GeocodingService.geocode(address);
  } catch(err) {
    debug(':: /search : geocoding ERROR -> ', err);
    return respond.failure('We couldn\'t geocode your address, sorry! Please try again!', err);
  }

  if (!latlng) { // address could not be geocoded
    return respond.failure('Address not found :(');
  }


  // step 2: search police api
  let crimes = []; // if there's no error, we default to empty array and always return it
  try {
    crimes = await CrimesService.search(latlng, date);
  } catch(err) { // catches errors, does not mean api returned results
    // NOTE: weirdly enough, if there date provided is not available (ex: Jan 2020, Jan 2016), the api returns 404
    debug(':: /search : crimes search api ERROR -> ', err.message);
    return respond.failure('We couldn\'t search for crimes, sorry! Please try again!', err.message);
  };

  return respond.success(crimes, latlng);
});

module.exports = router;
