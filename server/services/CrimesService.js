const HTTPService = require('./HTTPService');

module.exports = {
  constructSearchUrl(lat, lng, date = '') {
    // FIXME: enumerate as a const
    let url = `https://data.police.uk/api/crimes-street/all-crime?lat=${lat}&lng=${lng}`;
    if (date) {
      url += `&date=${date}`;
    }
    return url;
  },

  async search({ lat, lng } = {}, date) {
    const url = this.constructSearchUrl(lat, lng, date);
    return HTTPService.get(url).then((response) => response.data); // unwrap axios data
  }
};