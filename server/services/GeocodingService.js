const GoogleMaps = require('@google/maps');
const debug = require('debug')('crimes:services:geocoding');

if (!process.env.GOOGLE_MAPS_API_KEY) {
  debug('ERROR -> environmental variable for Google Maps API KEY is missing!');
  // FIXME: throw an error (left like this to test it first on the EC2 instance ;))
}

module.exports = {
  createGoogleMapsClient() {
    return GoogleMaps.createClient({
      key: process.env.GOOGLE_MAPS_API_KEY,
      Promise: Promise
    });
  },
  async geocode(address) { // TODO: for simplicity reasons there are no param checks; if wrong -> return falsey Promise
    return this.createGoogleMapsClient()
      .geocode({ address })
      .asPromise()
      .then((response) => {
        const locations = response.json.results;
        if (locations && locations.length) {
          return locations[0].geometry.location;
        }
        return null; // response was okay, but address could not be found; not an error -> handle it properly
      });
  }
};