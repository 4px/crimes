const axios = require('axios');

// Simple wrapper for easy stubbing
module.exports = {
  async get(url) {
    return axios.get(url);
  }
};