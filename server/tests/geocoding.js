const expect = require('chai').expect;
const sinon = require('sinon');
const GeocodingService = require('../services/GeocodingService');

describe('Geocoding Service', () => {
  describe('General availability', () => {
    it('should be available', () => {
      expect(GeocodingService).to.exist;
    });
  
    it('should contain createGoogleMapsClient function', () => {
      expect(GeocodingService.createGoogleMapsClient).to.be.a('function');
    });
  
    it('should contain geocode function', () => {
      expect(GeocodingService.geocode).to.be.a('function');
    });
  });
  
  describe('Perform geocoding', () => {
    before(() => {
      sinon.replace(GeocodingService, 'createGoogleMapsClient', sinon.fake.returns({
        geocode: () => ({
          asPromise: () => Promise.resolve({
            json: {
              results: [{
                geometry: {
                  location: {
                    lat: 10,
                    lng: 10
                  }
                }
             }]
            }
          })
        })
      }));
    });
    after(() => {
      sinon.restore();
    });

    it('should return a Promise', () => {
      expect(GeocodingService.geocode() instanceof Promise).to.be.true;
    });

    it('should return lat/lng object', async () => {
      const result = await GeocodingService.geocode();
      expect(result).to.be.an('object');
      expect(result).to.include.all.keys('lat', 'lng');
    })
  });
});