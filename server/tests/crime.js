const expect = require('chai').expect;
const sinon = require('sinon');
const HTTPService = require('../services/HTTPService');
const CrimesService = require('../services/CrimesService');

describe('Crime Service', () => {
  describe('General availability', () => {
    it('should be available', () => {
      expect(CrimesService).to.exist;
    });
  
    it('should contain createGoogleMapsClient function', () => {
      expect(CrimesService.constructSearchUrl).to.be.a('function');
    });
  
    it('should contain geocode function', () => {
      expect(CrimesService.search).to.be.a('function');
    });
  });
  
  describe('Perform search', () => {
    before(() => {
      sinon.replace(HTTPService, 'get', sinon.fake.returns(Promise.resolve({ data: [{}] })));
    });
    after(() => {
      sinon.restore();
    });
    
    it('should return a Promise', () => {
      expect(CrimesService.search() instanceof Promise).to.be.true;
    });

    it('should return array with one item', async () => {
      const result = await CrimesService.search();
      expect(result).to.be.an('array').that.is.not.empty;
    })
  });
});