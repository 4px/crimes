const path = require('path');
const logger = require('morgan');
const express = require('express');
const cookieParser = require('cookie-parser');

// we use .env for environmental vairables locally, but might be using OS env variables directly
// therefore we don't track the result of this operation (config.error would be true if file's missing; config.result -> parsed data)
const config = require('dotenv').config();

const APIRouter = require('./routes');
const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.resolve('./frontend/crimes/dist')));

app.use('/api', APIRouter);

module.exports = app;
