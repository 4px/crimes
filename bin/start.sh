#!/usr/bin/env bash
source /home/ubuntu/.env
export GOOGLE_MAPS_API_KEY
cd /home/ubuntu/crimes-app
forever stopall
PORT=80 forever start ./bin/www