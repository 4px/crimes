#!/usr/bin/env bash
# there was this awful issue that after a deploy was successful, the next one isn't
# and the error is that some of the directories are not empty
# this is what I could find as a fix :)

# create swap as ng build --prod could not finish in t2.micro instance :(
sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
sudo /sbin/mkswap /var/swap.1
sudo chmod 600 /var/swap.1
sudo /sbin/swapon /var/swap.1

sudo rm -rf /home/ubuntu/crimes-app/*
sudo npm install forever -g